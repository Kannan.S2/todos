# todos App

## Getting Started

Welcome to the `todos` application! This project serves as a simple introduction to setting up a Node.js application and learning the basics of Docker. Follow the steps below to get started:


1. Clone the repository:
   ```bash
   git clone https://gitlab.com/Kannan.S2/todos.git
   cd todos
   ```

## Building the Docker Image

To build the Docker image, follow these steps:

1. Create a Dockerfile in the project directory:

   ```Dockerfile
   # syntax=docker/dockerfile:1
   FROM node:18-alpine
   WORKDIR /app
   COPY . .
   RUN yarn install --production
   CMD ["node", "src/index.js"]
   EXPOSE 3000
   ```

2. Build the Docker image:

   ```bash
   docker build -t kannans/todos .
   ```

   For a specific platform:

   ```bash
   docker build --platform linux/amd64 -t YOUR-USER-NAME/todos .
   ```

## Running the Docker Image

To run the Docker image, use the following commands:

- Run in the background:

  ```bash
  docker run -dp 127.0.0.1:3000:3000 kannans/todos
  ```

- Run interactively:

  ```bash
  docker run -it -p 127.0.0.1:3000:3000 kannans/todos
  ```

## Managing Docker Containers

- View running containers:

  ```bash
  docker ps
  ```

- Stop and remove a container:

  ```bash
  docker stop <the-container-id>
  docker rm <the-container-id>
  ```

  Force remove:

  ```bash
  docker rm -f <the-container-id>
  ```

## Pushing Docker Images to Docker Hub

1. Create a new tag:

   ```bash
   docker tag kannans/todos YOUR-USER-NAME/todos
   ```

2. Push to Docker Hub:

   ```bash
   docker push YOUR-USER-NAME/todos
   ```

## Persistent Storage

Create a Docker volume for persistent storage:

```bash
docker volume create todo-db
```

Run the Docker image with the mounted volume:

```bash
docker run -dp 127.0.0.1:3000:3000 --mount type=volume,src=todo-db,target=/etc/todos kannans/todos:latest
```

## Using Bind Mounts for Development

For local development and working in development mode:

```bash
docker run -dp 127.0.0.1:3000:3000 \
    -w /app --mount type=bind,src="$(pwd)",target=/app \
    node:18-alpine \
    sh -c "yarn install && yarn run dev"
```

### Todo AppScreen
![todo app](src/static/img/todos.png)


# Dockerized Todo Application with MySQL

Guide for setting up a Todo application with a MySQL database using Docker containers.

## Step 1: Create Docker Network

```bash
docker network create todo-app
```

Create a Docker network named `todo-app` to facilitate communication between containers.

## Step 2: Run MySQL Docker Container

```bash
docker run -d \
    --network todo-app --network-alias mysql \
    -v todo-mysql-data:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=secret \
    -e MYSQL_DATABASE=todos \
    mysql:8.0
```

Pull the MySQL image, run the container on the `todo-app` network with an alias `mysql`. This alias serves as the host URL for connecting to MySQL.

## Step 3: Verify MySQL Container

Access the MySQL instance and verify functionality:

```bash
docker exec -it <mysql-container-id> mysql -u root -p
```

Enter the password (`secret`) when prompted, and execute the following MySQL commands:

```sql
mysql> SHOW DATABASES;
mysql> exit
```

## Step 4: Run Node Application with MySQL

```bash
docker run -dp 127.0.0.1:3000:3000 \
  -w /app -v "$(pwd):/app" \
  --network todo-app \
  -e MYSQL_HOST=mysql \
  -e MYSQL_USER=root \
  -e MYSQL_PASSWORD=secret \
  -e MYSQL_DB=todos \
  node:18-alpine \
  sh -c "yarn install && yarn run dev"
```

Run a Node.js application in a Docker container, connecting to the MySQL container using specified environment variables.

Adjust `MYSQL_USER`, `MYSQL_PASSWORD`, and `MYSQL_DB` according to your application's needs.

## Step 5: Verify Docker Logs

```bash
docker logs <the-container-id>
```

Check the Docker logs to ensure successful application startup and MySQL connectivity:

```
[nodemon] starting `node src/index.js`
Waiting for mysql:3306.
Connected!
Connected to mysql db at host mysql
Listening on port 3000
```

Your Todo application should now be accessible at `http://127.0.0.1:3000`. Customize the configuration based on your application's requirements.

# Docker Compose Setup for Multi-Container Applications

[Docker Compose](https://docs.docker.com/compose/) simplifies the orchestration of multi-container applications by allowing you to define services in a YAML file. With a single command, you can effortlessly manage the deployment and removal of your application stack.

## Getting Started

### Step 1: Create Docker Compose File

In your application directory, create a file named `compose.yaml`. This file will serve as the blueprint for your multi-container setup.

### Step 2: Define Services

Within `compose.yaml`, define the services for your application stack. At a minimum, you'll need to define services for your application and MySQL. Here's a basic example:

```yaml
services:
  app:
    image: node:18-alpine
    working_dir: /app
    volumes:
      - ./:/app
   ......
```

### Step 3: Start the Application Stack

Run the following command to start the application stack. The `-d` flag runs the services in the background.

```bash
$ docker-compose up -d
```

### Step 4: View Logs

To view the logs of your running services, use the following command:

```bash
$ docker-compose logs -f
```

This command will display real-time logs for all services in your application stack.

That's it! Your multi-container application stack is up and running. Customize the `compose.yaml` file according to your application's requirements, and enjoy the simplicity of managing your services with Docker Compose.

-----

Feel free to explore, modify, and learn with this simple `todos` application! If you have any questions or run into issues, please refer to the documentation or reach out for support.


Happy coding!